const { createApp } = Vue;
createApp({
    data() {
        return {
            username: "",
            password: "",
            error: null,
            sucesso: null,
            userAdmin: false,

            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            usuarios: ["admin69@gmail.com", "kerolly@gmail.com", "bassib@gmail.com"],
            senhas: ["1234", "1234", "1234"],
            mostrarEntrada: false,
            mostrarLista: false,
            mostrarMenu: false,
        };
    },//Fechamento data

    methods: {
        login() {
            this.mostrarEntrada = false;
            if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            setTimeout(() => {
                this.mostrarEntrada = true;

                const index = this.usuarios.indexOf(this.username);
                if (index !== -1 && this.senhas[index] === this.password) {
                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso";
                    if (this.username === "admin69@gmail.com" && index === 0) {
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    }
                } //Fechamento do if
                else {
                    this.error = "Nome ou senha incorreta!";
                    this.sucesso = null;
                } //fechamento else

                this.email = "";
                this.password = "";
            }, 500);
        },

        paginaCadastro() {
            this.mostrarEntrada = false;
            if (this.userAdmin == true) {
                this.sucesso = "Login ADM indentificado!";
                this.error = null;
                this.mostrarEntrada = true;

                //localStorage.setItem("username", this.username);
                //localStorage.setItem("password", this.password);
                setTimeout(() => { }, 2000);

                setTimeout(() => {
                    window.location.href = "paginaCadastro.html";
                }, 1000); //FechamentosetTimeout
            } else {
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "Faça login com usúario administrativo";
                }, 500);//Fechamento setTimeout
            }//Fechamento else
        },//fechamento paginaCadastro
        
        adicionarUsuario() {
            //this.username = localStorage.getItem("username");
            //this.password = localStorage.getItem("password");
          
            if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
                  this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                  this.senhas = JSON.parse(localStorage.getItem("senhas"));
                } //Fechamento if
                
                this.mostrarEntrada = false;
          
                setTimeout(() => {
                  this.mostrarEntrada = true;
          
                  if (this.newUsername !== "") {
                    if (!this.usuarios.includes(this.newUsername)) {
                      if (this.newPassword && this.newPassword === this.confirmPassword) {
                        this.usuarios.push(this.newUsername);
                        this.senhas.push(this.newPassword);
          
                        //Armazenamento do usuário e senha no LocalStorage
                        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));          
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));
          
                        this.error = null;
                        this.sucesso = "Usuário cadastrado com sucesso!";
                      } //Fechamento do if
                      else {
                        this.sucesso = null;
                        this.error = "Por favor, confirme sua senha!!!";
                      } //Fechamento do else
                    } //Fechamento if includes
                    else {
                      this.sucesso = null;
                      this.error = "O usuário informado já está cadastrado!";
                    } //Fechamento do else
                  } //Fechamento if !== ""
                  else {
                    this.error = "Por favor, digite um nome de usuário!";
                    this.sucesso = null;
                  } //Fechamento do else
          
                  this.newUsername = "";
                  this.newPassword = "";
                  this.confirmPassword = "";
                  this.username = "";
                  this.password = "";
                }, 500); //Fechamento setTimeout
              }, //Fechamento adicionarUsuario
        
        verCadastrados() {            
            if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento verCadastrados

        excluirUsuario(usuario) {
            this.mostrarEntrada = false;
            if(usuario === "admin") {
                //impedindo a exclusão de usuarios
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null
                    this.error = "Usuario ADMIN não pode ser excluído!!!";
                },500);
                return;//Força a finalização do bloco/ função
            }//Fechamento if usúario

            if(index !== -1) {
                this.usuarios.splice(index, 1);
                this.senhas.splice(index, 1);

                //atualização dos vetores no localStorage
                localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                localStorage.setItem("senhas", JSON.stringify(this.senhas));
            }
        },
    }//Fechamento methods
}).mount("#app");
