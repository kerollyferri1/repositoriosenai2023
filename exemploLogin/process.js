const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            //Variaveis para armazenamento das entradas do formulario da pagina cadastro 
            newUsername:"",
            newPassword:"",
            confirmPassword:"",

            //Dclaração dos arrays para armazenamento de usuários e senhas

            usuarios: ["admin", "kerolly", "dudinha"],
            senhas: ["1234", "1234", "1234"],
            MostrarEntradas: false,
            mostrarLista: false,
        }
    },//Fechamento data

    methods:{
        login(){
          setTimeout(() => {
            //alert("Dentro do setTimeout!");
            if((this.username === "Kerolly" && this.password === "12342023") ||
            (this.username === "Dudinha" && this.password === "12345678") || 
            (this.username === "Admin" && this.password === "admin12345")){
                this.sucesso = "Login efetuado com sucesso!";
                this.error = null;
                if(this.username === "Admin"){
                    this.userAdmin = true;
                }
            }
            else{
                // alaert("Login não efetuado!");
                this.error = "Nome ou senha incorretos!";
                this.sucesso = null;
            }
          }, 1000);
          //alert("Saiu do setTimeout!!!")
        },//Fechamento login

        login2(){
            this.MostrarEntradas = false;

            if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
            this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
            this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
            setTimeout(() => {
                this.MostrarEntradas = true;

                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    
                    localStorage.setItem("username" , this.username);
                    localStorage.setItem("password" , this.password);
                    
                    this.error = null;
                    this.sucesso = "Login 2 efetuado com sucesso";
                    if(this.username === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    }
                }//Fechamento if 
                else{
                    this.error = "2 - Nome ou senha incorretos!";
                    this.sucesso = null;
                }//Fechamento else 

                this.username = "";
                this.password = "";
   
            }, 500);
        },//Fechamento Login2

        paginaCadastro(){
            this.MostrarEntradas = false;
            if(this.userAdmin == true){
                this.sucesso = "Login ADM identificado!";
                this.error = null;
                this.MostrarEntradas = true;

                // localStorage.setItem("username" , this.username);
                // localStorage.setItem("password" , this.password);

                setTimeout(() => {}, 2000); 

                setTimeout(() => {
                    window.location.href = "paginaCadastro.html";
                },1000);//Fechamento setTimeout
            }  
            else{
                setTimeout(() => {
                    this.MostrarEntradas = true;
                    this.sucesso = null;
                    this.error ="Faça login com usuário administrativo!";
                }, 500);//Fechamento setTimeout
            }//Fechamento else
        },//Fechamento paginaCadastro

        adicionarUsuario(){

            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");
            this.MostrarEntradas = false;

            setTimeout(() => {
                this.MostrarEntradas = true;
                if(this.username === "admin"){
                    if(this.newUsername !== ""){
                        if(!this.usuarios.includes(this.newUsername)){
                            if(this.newPassword && this.newPassword === this.confirmPassword){
                                this.usuarios.push(this.newUsername);
                                this.senhas.push(this.newPassword);

                                //Armazenamento do usuário e senha no LocalStorage
                                localStorage.setItem("usuarios", JSON.stringify(this.usuarios));

                                localStorage.setItem("senhas", JSON.stringify(this.senhas));
  
                                this.error = null;
                                this.sucesso = "Usuário cadastrado com sucesso!"; 
                            }//Fechamento do If
                            else{
                                this.sucesso = null;
                                this.error = "Por favor, confirme sua senha!!!";
                            }//Fechamento do else 
                        }//Fechamento If includes
                        else{
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado!";
                        }//Fechamento do else
                    }//Fechamento If !==""
                    else{
                        this.error = "Por favor digite um nome de usuário!";
                        this.sucesso = null;
                    }//Fechamento do else
                }//Fechamento do if 
                else{
                    this.error = "Usuário NÃO ADM!!!";
                    this.sucesso = null;
                }//Fechamento else

                this.newUsername = "";
                this.newPassword = "";
                this.confirmPassword = "";
                
            }, 500);//Fechamento setTimeout 
        },//Fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento If
            this.mostrarLista  = !this.mostrarLista;
        },//Fechamento verCadastrados
        
        excluirUsuario(usuario){
            this.MostrarEntradas = false;
            if(usuario === "admin"){
                //impedir a exclusão do admin
                setTimeout(() => {
                    this.MostrarEntradas = true;
                    this.sucesso = null;
                    this.error = "O usuario ADMIN não pode ser excluido!!!";
                }, 500);
                return;//Força a finalização 
            }//Fechamento if

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index,1);

                    //Atualização dos vetores no LocalStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }//Fechamento index
            }//Fechamento do if cofirm 
        },//Fechamento excluirUsuario
    },//Fechamento methods

}).mount("#app"); 